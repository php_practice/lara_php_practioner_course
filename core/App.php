<?php

/**
* 
*/

namespace App\Core;

class App
{
	protected static $registry = [];

	public static function bind($key, $parameter)
	{
		static::$registry[$key]	= $parameter;
	}

	public static function get($key){

			if(! array_key_exists($key, static::$registry)){
				throw new Exception("the {$key} is not rectified", 1);
				
			}

	   return static::$registry[$key];
	}
}