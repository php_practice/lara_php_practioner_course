<?php

namespace App\Core;

class Router
{
    protected $routes = [

        'GET' => [],
        'POST' => []

    ];

    public static function load($file)
    {
        $router = new static;

        require $file;

        return $router;
    }

    public function get($uri, $controllers){
        $this->routes['GET'][$uri] = $controllers;
    }

    public function post($uri, $controllers){
        $this->routes['POST'][$uri] = $controllers;
    }

    public function direct($uri, $requestType)
    {
        // die(var_dump($this->routes[$requestType][$uri]));
        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callMethod(
                ...explode('@', $this->routes[$requestType][$uri])
                );
        }

        throw new Exception('No route defined for this URI.'. $uri);
    }

    public function callMethod($controller, $action)
    {
        $controller = "App\\Controllers\\{$controller}";

        $controller = new $controller;

        if(! method_exists($controller, $action)){
            throw new Exception("the {$controller} may not contain the method {$action}");
        }

        return $controller->$action();
    }
}