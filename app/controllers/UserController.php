<?php

/**
* 
*/
namespace App\Controllers;

use App\Core\App;

class UserController
{
	public function index()
	{
		$users = App::get('database')->selectAll('users');

		//view('index', ['users' => $users]);
		view('users', compact('users'));
	}

	public function store()
	{
		App::get('database')->storeName(
	'users', 
	[
		'name' => $_POST['name']
	]
	);

	header('location: users');
	}
}