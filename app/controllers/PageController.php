<?php

/**
* 
*/
namespace App\Controllers;

class PageController
{
	
	public function home()
	{
		view('index');
	}

	public function about()
	{
		$company = 'laracasts';
		view('about' , ['company' => $company]);
	}

	public function contact()
	{
		view('contact');	
	}
}